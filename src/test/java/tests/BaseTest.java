package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class BaseTest {
    private WebDriver driver;

    public BaseTest() {
    }

    @BeforeClass
    public void beforeClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void beforeTest() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        driver.manage().window().maximize();
    }

    protected WebDriver getDriver() {
        return driver;
    }

    public void navigateToSite(String site) {
        if (site.equalsIgnoreCase("demoqa")) {
            driver.get("https://demoqa.com/");
        } else if (site.equalsIgnoreCase("heroku")) {
            driver.get("https://the-internet.herokuapp.com/login");
        } else {
            System.out.println("Некорректное имя сайта");
        }
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
    }
}

