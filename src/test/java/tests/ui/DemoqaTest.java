package tests.ui;

import Model.MainPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.BaseTest;

public class DemoqaTest extends BaseTest {
    @BeforeMethod
    public void initSite() {
        navigateToSite("demoqa");
    }

    @Test
    public void alertsPromptTest() {
        MainPage mainPage = new MainPage(getDriver());
        String result = mainPage.clickAlertsFrame().clickAlerts().clickPromtButton().switchToAlert("Igor").getPromptResult();
        Assert.assertEquals(result, "You entered Igor");
    }

    @Test
    public void alertsConfirmTest() {
        MainPage mainPage = new MainPage(getDriver());
        String confirmResult = mainPage.clickAlertsFrame().clickAlerts().clickConfirmButton().alertAccept().getConfirmResult();
        Assert.assertEquals(confirmResult, "You selected Ok");
    }
}