package Model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BaseModel {

    public MainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h5[text() = 'Alerts, Frame & Windows']/ancestor::div[contains(@class, 'card mt-4 top-card')]")
    private WebElement alertsFrame;

    public AlertsPage clickAlertsFrame() {
        alertsFrame.click();
        return new AlertsPage(getDriver());
    }

}

