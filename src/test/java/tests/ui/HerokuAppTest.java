package tests.ui;

import Model.SecondMain;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.BaseTest;

public class HerokuAppTest extends BaseTest {
    @BeforeMethod
    public void initSite() {
        navigateToSite("heroku");
    }

    @Test
    public void loginConfirmTest() {
        SecondMain secondMain = new SecondMain(getDriver());
        String username = secondMain
                .insertUserName("tomsmith")
                .insertPassword("SuperSecretPassword!")
                .login()
                .getLoginResult();
        Assert.assertEquals(username, "You logged into a secure area!");


    }
    @Test
    public void logOutTest() {
        SecondMain secondMain = new SecondMain(getDriver());
        String logout = secondMain
                .insertUserName("tomsmith")
                .insertPassword("SuperSecretPassword!")
                .login()
                .logOut()
                .getLogOutResult();
        Assert.assertEquals(logout, "You logged out of the secure area!");
    }
}
