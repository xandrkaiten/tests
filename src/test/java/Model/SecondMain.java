package Model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SecondMain extends BaseModel {
    public SecondMain(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "username")
    private WebElement user;

    @FindBy(id = "password")
    private WebElement pass;

    @FindBy(xpath = "//*[@id='login']/button")
    private WebElement login;

    @FindBy(id = "flash")
    private WebElement getLoginResult;
    @FindBy(id = "flash")
    private WebElement getLogOutResult;

    @FindBy(xpath = "//a[@href='/logout']")
    private WebElement logOut;

    public SecondMain insertUserName(String user1) {
        user.sendKeys(user1);
        return new SecondMain(getDriver());
    }


    public SecondMain insertPassword(String psw) {
        pass.sendKeys(psw);
        return new SecondMain(getDriver());

    }

    public SecondMain login() {
        login.click();
        return this;
    }


    public SecondMain logOut() {
        logOut.click();
        return this;
    }
    public String getLoginResult() {
        return getLoginResult.getText().split("\\n")[0];
    }
    public String getLogOutResult() {
        return getLogOutResult.getText().split("\\n")[0];
    }
}
