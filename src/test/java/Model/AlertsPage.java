package Model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AlertsPage extends BaseModel {

    public AlertsPage(WebDriver driver, WebElement alerts, WebElement promptResult, WebElement promtButton) {
        super(driver);
        this.alerts = alerts;
        this.promptResult = promptResult;
        this.promtButton = promtButton;
    }

    @FindBy(id = "confirmButton")
    private WebElement confirmButton;
    @FindBy(xpath = "//span[@class='text' and text()='Alerts']")
    private WebElement alerts;
    @FindBy(id = "promptResult")
    private WebElement promptResult;
    @FindBy(id = "promtButton")
    private WebElement promtButton;
    @FindBy(id = "confirmResult")
    private WebElement confirmResult;

    public AlertsPage(WebDriver driver) {
        super(driver);
    }

    public AlertsPage clickAlerts() {
        alerts.click();
        return this;
    }

    public AlertsPage clickConfirmButton() {
        confirmButton.click();
        return this;
    }

    public AlertsPage clickPromtButton() {
        promtButton.click();
        return this;
    }

    public String getPromptResult() {
        return promptResult.getText();
    }

    public String getConfirmResult() {
        return confirmResult.getText();
    }

    public AlertsPage alertAccept() {
        getDriver().switchTo().alert().accept();
        return this;
    }

    public AlertsPage switchToAlert(String str) {
        getDriver().switchTo().alert().sendKeys(str);
        alertAccept();
        return this;
    }
}

